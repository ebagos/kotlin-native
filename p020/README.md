# 問題 007

n × (n - 1) × ... × 3 × 2 × 1 を n! と表す．

例えば， 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800 となる．

この数の各桁の合計は 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27 である．

では， 100! の各桁の数字の和を求めよ．

# 問題点

Kotlinのプリミティグ型を使用した通常の計算では回答不能、というのが正解

数値の各桁を要素とした配列で表現し、演算すればメモリのある限り桁数を増やすことができる
